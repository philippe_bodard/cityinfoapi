﻿using CityInfo.API.Models;
using System;
using System.Collections.Generic;

namespace CityInfo
{
    public class CitiesDataStore
    {
        public static CitiesDataStore Current { get; } = new CitiesDataStore();

        public List<CityDto> Cities { get; set; }

        public CitiesDataStore()
        {
            // init dummy data
            Cities = new List<CityDto>()
            {
                new CityDto()
                {
                     Id = 1,
                     Name = "New York City",
                     Description = "The one with that big park.",
                     PointsOfInterest = new List<PointOfInterestDto>()
                     {
                         new PointOfInterestDto()
                         {
                             Id =1,
                             Name = "Central Park",
                             Description = "Central Park with its zoo and forest"
                         },
                         new PointOfInterestDto()
                         {
                             Id =2,
                             Name = "Apple Store",
                             Description = "Biggest apple store in the world"
                         }

                     }
                },
                new CityDto()
                {
                    Id = 2,
                    Name = "Antwerp",
                    Description = "The one with the cathedral that was never really finished.",
                    PointsOfInterest = new List<PointOfInterestDto>()
                    {
                        new PointOfInterestDto()
                        {
                            Id =1,
                            Name = "Cathedral",
                            Description = "Cathedral which looks like the one from Paris but with only one tower"
                        },

                    }

                },
                new CityDto()
                {
                    Id= 3,
                    Name = "Paris",
                    Description = "The one with that big tower.",
                    PointsOfInterest = new List<PointOfInterestDto>()
                    {
                        new PointOfInterestDto()
                        {
                            Id =1,
                            Name = "Cathedral",
                            Description = "Cathedral with two towers"
                        },

                    }


                }
            };
        }

    }

}
    

