﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CityInfo.API.Models;
using CityInfo.API.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CityInfo.API.Controllers
{
    [Route("api/Cities")]

    public class CitiesController : ControllerBase
    {

        private ICityInfoRepository _cityInfoRepository;

        private IMapper _mapper;


        public CitiesController(ICityInfoRepository cityInfoRepository,
            IMapper mapper)
        {
            _cityInfoRepository = cityInfoRepository ??
                throw new ArgumentNullException(nameof(cityInfoRepository));

            _mapper = mapper ?? 
                throw new ArgumentNullException(nameof(mapper));

        }



        [HttpGet]
        public IActionResult GetCities()
        {

            var cityEntities = _cityInfoRepository.GetCities();

            return Ok(_mapper.Map<IEnumerable<CityWithoutPointsOfInterestDto>>(cityEntities)); ;
        }

        [HttpGet("{id}")]
        public IActionResult GetCity(int id, bool includePointsOfInterest = false)
        {

            if (!_cityInfoRepository.CityExists(id))
            {
                return NotFound();
            }

            var cityEntity = _cityInfoRepository.GetCity(id, includePointsOfInterest);

         
            if (includePointsOfInterest)
            {
                return Ok(_mapper.Map<CityDto>(cityEntity));

            } else
            {
                return Ok(_mapper.Map<CityWithoutPointsOfInterestDto>(cityEntity));
            }
        }

    }
}
